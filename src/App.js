import React, { Fragment, useState, useEffect } from "react";

import "./App.css";
import { Header } from "./components/header";

import { Wrapper } from "./components/wrapper";

const App = () => {
  const memoryTask = localStorage.getItem("tasks");
  const [nickName, setNickName] = useState("");
  const [realName, setRealName] = useState("");
  const [superpowers, setSuperSowers] = useState("");
  const [originDescription, setOriginDescription] = useState("");
  const [catchPhrase, setCatchPhrase] = useState("");

  const [list, setList] = useState(memoryTask ? JSON.parse(memoryTask) : []);

  useEffect(() => {
    localStorage.setItem("superheroList", JSON.stringify(list));
  }, [list]);
  const onAddTask = () => {
    const task = {
      nickName,
      realName,
      catchPhrase,
      superpowers,
      originDescription,
      id: Math.round(Math.random() * 1000),
    };
    setOriginDescription("");
    setNickName("");
    setRealName("");
    setSuperSowers("");
    setCatchPhrase("");
    const _list = [...list];
    _list.push(task);
    setList(_list);
  };
  const onDeleteTask = (id) => {
    const _list = [...list];
    const index = _list.findIndex((task) => task.id === id);
    _list.splice(index, 1);
    setList(_list);
  };
  const onEditCatchPhrase = (e, id) => {
    const _list = [...list];
    const index = _list.findIndex((task) => task.id === id);

    _list[index].catchPhrase = e.target.value;
    setList(_list);
  };
  const onEditOriginDescription = (e, id) => {
    const _list = [...list];
    const index = _list.findIndex((task) => task.id === id);
    _list[index].originDescription = e.target.value;
    setList(_list);
  };

  const renderTasks = () =>
    list.map(
      (
        { nickName, superpowers, realName, originDescription, catchPhrase, id },
        i
      ) => (
        <ul key={id}>
          <img src="" alt="" />
          <li>
            <h5>Nick Name:</h5>
            <input className="inputText" value={nickName} />
          </li>
          <li>
            <h5>Real Name:</h5>
            <input className="inputText" value={realName} />
          </li>
          <li>
            <h5>SuperSowers:</h5>
            <input className="inputText" value={superpowers} />
          </li>
          <li>
            <h5>Origin Description:</h5>
            <textarea
              className="inputText"
              value={originDescription}
              onChange={(e) => onEditOriginDescription(e, id)}
            />
          </li>

          <li>
            <h5>Catch Phrase:</h5>
            <textarea
              className="inputText"
              value={catchPhrase}
              onChange={(e) => onEditCatchPhrase(e, id)}
            />
          </li>
          <button onClick={() => onDeleteTask(id)}>delete</button>
          <hr />
        </ul>
      )
    );
  return (
    <Fragment>
      <Header />
      <Wrapper>
        <div className="flex">
          <div className="formList">
            <input
              className="name"
              placeholder="nickname"
              value={nickName}
              onChange={(e) => setNickName(e.target.value)}
            />
            <input
              className="name"
              placeholder="real name"
              value={realName}
              onChange={(e) => setRealName(e.target.value)}
            />
            <input
              className="name"
              placeholder="super powers"
              value={superpowers}
              onChange={(e) => setSuperSowers(e.target.value)}
            />
            <textarea
              value={originDescription}
              onChange={(e) => setOriginDescription(e.target.value)}
              className="description"
              placeholder="origin Description"
            />
            <textarea
              value={catchPhrase}
              onChange={(e) => setCatchPhrase(e.target.value)}
              className="description"
              placeholder="Catch Phrase"
            />
            
            <button onClick={onAddTask}>Add</button>
          </div>
          <div className="superheroList">{renderTasks()}</div>
        </div>
      </Wrapper>
    </Fragment>
  );
};
export default App;
